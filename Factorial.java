// Program that calculates the factorial of a number.
import java.util.Scanner;
// Class that computes the factorial of a number.
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 // Check and handle a negative number input.
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
// Compute the factorial of the number input.
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
